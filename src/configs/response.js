export const responseData = (res, data, message, statusCode) => {
  res.status(statusCode).json({
    message,
    content: data,
    date: new Date(),
  });
};
