import express from "express";
import cors from "cors";
import { rootRoute } from "./routes/rootRoute.js";
import swaggerUi from "swagger-ui-express";
import fs from "fs";
import YAML from "yaml";

const app = express();
app.use(cors());
app.use(express.json());
app.listen(8080, () => {
  console.log("MY SEVER IS RUNNING AT", 8080);
});
app.use(rootRoute);

const folderPath = "./swagger/";
const files = fs.readdirSync(folderPath);
let combinedYAMLContent = "";

files.forEach((file) => {
  const content = fs.readFileSync(folderPath + file, "utf8");
  combinedYAMLContent += content;
});

const swaggerDocument = YAML.parse(combinedYAMLContent);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
