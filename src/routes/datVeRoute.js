import express from "express";
import {
  DatVe,
  LayDanhSachPhongVe,
  TaoLichChieu,
} from "../controllers/datVeController.js";

export const datVeRoute = express.Router();

datVeRoute.post("/DatVe", DatVe);
datVeRoute.get("/LayDanhSachPhongVe", LayDanhSachPhongVe);
datVeRoute.post(
  "/TaoLichChieu/:maPhim/:ngayChieuGioChieu/:maRap/:giaVe",
  TaoLichChieu
);
