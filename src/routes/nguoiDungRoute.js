import express from "express";
import {
  CapNhatThongTinNguoiDung,
  DangKi,
  DangNhap,
  LayDanhSachLoaiNguoiDung,
  LayDanhSachNguoiDung,
  TimKiemNguoiDung,
  XoaNguoiDung,
} from "../controllers/nguoiDungController.js";

export const nguoiDungRoute = express.Router();

nguoiDungRoute.get("/LayDanhSachNguoiDung", LayDanhSachNguoiDung);
nguoiDungRoute.post("/DangNhap", DangNhap);
nguoiDungRoute.post("/DangKi", DangKi);
nguoiDungRoute.get("/LayDanhSachLoaiNguoiDung", LayDanhSachLoaiNguoiDung);
nguoiDungRoute.get("/TimKiemNguoiDung", TimKiemNguoiDung);
nguoiDungRoute.put("/CapNhatThongTinNguoiDung", CapNhatThongTinNguoiDung);
nguoiDungRoute.delete("/XoaNguoiDung/:taiKhoan/:matKhau", XoaNguoiDung);
