import express from "express";
import {
  LayDanhSachLichChieu,
  LayThongTinCumRapTheoHeThong,
  LayThongTinHeThongRap,
  ThemCumRap,
} from "../controllers/rapController.js";

export const rapRoute = express.Router();

rapRoute.get("/LayDanhSachLichChieu", LayDanhSachLichChieu);
rapRoute.get("/LayThongTinHeThongRap", LayThongTinHeThongRap);
rapRoute.get("/LayThongTinCumRapTheoHeThong", LayThongTinCumRapTheoHeThong);
rapRoute.post("/ThemCumRap", ThemCumRap);
