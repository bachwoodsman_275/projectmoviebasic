import express from "express";
import {
  LayDanhSachBanner,
  LayDanhSachPhim,
  LayDanhSachPhimPhanTrang,
  LayDanhSachPhimTheoNgay,
  ThemPhimUploadHinh,
} from "../controllers/phimController.js";

import multer from "multer";
import path from "path";

const storage = multer.diskStorage({
  destination: "./src/image",
  filename: (req, file, cb) => {
    return cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`
    );
  },
});
const upload = multer({ storage: storage });

export const phimRoute = express.Router();
phimRoute.get("/LayDanhSachPhim", LayDanhSachPhim);
phimRoute.get("/LayDanhSachBanner", LayDanhSachBanner);
phimRoute.get("/LayDanhSachPhimPhanTrang", LayDanhSachPhimPhanTrang);
phimRoute.get("/LayDanhSachPhimTheoNgay", LayDanhSachPhimTheoNgay);
phimRoute.post(
  "/ThemPhimUploadHinh",
  upload.single("hinhAnh"),
  ThemPhimUploadHinh
);
