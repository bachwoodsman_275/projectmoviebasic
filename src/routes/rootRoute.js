import express from "express";
import { phimRoute } from "./phimRoute.js";
import { datVeRoute } from "./datVeRoute.js";
import { rapRoute } from "./rapRoute.js";
import { nguoiDungRoute } from "./nguoiDungRoute.js";

export const rootRoute = express.Router();
rootRoute.use("/api/QuanLyPhim", phimRoute);
rootRoute.use("/api/QuanLyDatVe", datVeRoute);
rootRoute.use("/api/QuanLyRap", rapRoute);
rootRoute.use("/api/QuanLyNguoiDung", nguoiDungRoute);
