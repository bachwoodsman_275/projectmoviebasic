import _sequelize from "sequelize";
const { Model, Sequelize } = _sequelize;

export default class Banner extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        ma_banner: {
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
        },
        ma_phim: {
          type: DataTypes.INTEGER,
          allowNull: true,
          references: {
            model: "Phim",
            key: "ma_phim",
          },
        },
        hinh_anh: {
          type: DataTypes.STRING(150),
          allowNull: true,
          references: {
            model: "Phim",
            key: "hinh_anh",
          },
        },
      },
      {
        sequelize,
        tableName: "Banner",
        timestamps: false,
        indexes: [
          {
            name: "PRIMARY",
            unique: true,
            using: "BTREE",
            fields: [{ name: "ma_banner" }],
          },
          {
            name: "ma_phim",
            using: "BTREE",
            fields: [{ name: "ma_phim" }],
          },
          {
            name: "hinh_anh",
            using: "BTREE",
            fields: [{ name: "hinh_anh" }],
          },
        ],
      }
    );
  }
}
