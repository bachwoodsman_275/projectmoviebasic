import _sequelize from "sequelize";
const { Model, Sequelize } = _sequelize;

export default class DatVe extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nguoi_dung_id: {
          type: DataTypes.INTEGER,
          allowNull: true,
          references: {
            model: "NguoiDung",
            key: "nguoi_dung_id",
          },
        },
        ma_lich_chieu: {
          type: DataTypes.STRING(150),
          allowNull: true,
          references: {
            model: "LichChieu",
            key: "ma_lich_chieu",
          },
        },
        ma_ghe: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "DatVe",
        timestamps: false,
        indexes: [
          {
            name: "nguoi_dung_id",
            using: "BTREE",
            fields: [{ name: "nguoi_dung_id" }],
          },
          {
            name: "ma_lich_chieu",
            using: "BTREE",
            fields: [{ name: "ma_lich_chieu" }],
          },
        ],
      }
    );
  }
}
