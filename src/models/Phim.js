import _sequelize from "sequelize";
const { Model, Sequelize } = _sequelize;

export default class Phim extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        ma_nhom: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        ma_phim: {
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
        },
        ten_phim: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        trailer: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        hinh_anh: {
          type: DataTypes.STRING(150),
          allowNull: true,
          unique: "hinh_anh",
        },
        mo_ta: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        ngay_khoi_chieu: {
          type: DataTypes.DATEONLY,
          allowNull: true,
        },
        danh_gia: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        hot: {
          type: DataTypes.BOOLEAN,
          allowNull: true,
        },
        dang_chieu: {
          type: DataTypes.BOOLEAN,
          allowNull: true,
        },
        sap_chieu: {
          type: DataTypes.BOOLEAN,
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "Phim",
        timestamps: false,
        indexes: [
          {
            name: "PRIMARY",
            unique: true,
            using: "BTREE",
            fields: [{ name: "ma_phim" }],
          },
          {
            name: "hinh_anh",
            unique: true,
            using: "BTREE",
            fields: [{ name: "hinh_anh" }],
          },
        ],
      }
    );
  }
}
