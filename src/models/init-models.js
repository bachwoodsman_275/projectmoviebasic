import _sequelize from "sequelize";
const DataTypes = _sequelize.DataTypes;
import _Banner from "./Banner.js";
import _CumRap from "./CumRap.js";
import _DatVe from "./DatVe.js";
import _Ghe from "./Ghe.js";
import _HeThongRap from "./HeThongRap.js";
import _LichChieu from "./LichChieu.js";
import _NguoiDung from "./NguoiDung.js";
import _Phim from "./Phim.js";
import _RapPhim from "./RapPhim.js";

export default function initModels(sequelize) {
  const Banner = _Banner.init(sequelize, DataTypes);
  const CumRap = _CumRap.init(sequelize, DataTypes);
  const DatVe = _DatVe.init(sequelize, DataTypes);
  const Ghe = _Ghe.init(sequelize, DataTypes);
  const HeThongRap = _HeThongRap.init(sequelize, DataTypes);
  const LichChieu = _LichChieu.init(sequelize, DataTypes);
  const NguoiDung = _NguoiDung.init(sequelize, DataTypes);
  const Phim = _Phim.init(sequelize, DataTypes);
  const RapPhim = _RapPhim.init(sequelize, DataTypes);

  RapPhim.belongsTo(CumRap, {
    as: "ma_cum_rap_CumRap",
    foreignKey: "ma_cum_rap",
  });
  CumRap.hasMany(RapPhim, { as: "danhSachRap", foreignKey: "ma_cum_rap" });
  DatVe.belongsTo(LichChieu, {
    as: "ma_lich_chieu_LichChieu",
    foreignKey: "ma_lich_chieu",
  });
  LichChieu.hasMany(DatVe, { as: "DatVes", foreignKey: "ma_lich_chieu" });
  DatVe.belongsTo(NguoiDung, { as: "nguoi_dung", foreignKey: "nguoi_dung_id" });
  NguoiDung.hasMany(DatVe, { as: "DatVes", foreignKey: "nguoi_dung_id" });
  Banner.belongsTo(Phim, { as: "ma_phim_Phim", foreignKey: "ma_phim" });
  Phim.hasMany(Banner, { as: "Banners", foreignKey: "ma_phim" });
  Banner.belongsTo(Phim, { as: "hinh_anh_Phim", foreignKey: "hinh_anh" });
  Phim.hasMany(Banner, { as: "hinh_anh_Banners", foreignKey: "hinh_anh" });
  LichChieu.belongsTo(Phim, { as: "ma_phim_Phim", foreignKey: "ma_phim" });
  Phim.hasMany(LichChieu, { as: "LichChieus", foreignKey: "ma_phim" });
  Ghe.belongsTo(RapPhim, { as: "ma_rap_RapPhim", foreignKey: "ma_rap" });
  RapPhim.hasMany(Ghe, { as: "Ghes", foreignKey: "ma_rap" });
  LichChieu.belongsTo(RapPhim, { as: "ma_rap_RapPhim", foreignKey: "ma_rap" });
  RapPhim.hasMany(LichChieu, { as: "LichChieus", foreignKey: "ma_rap" });
  CumRap.belongsTo(HeThongRap, {
    as: "ma_cum_rap_Rap",
    foreignKey: "ma_cum_rap",
  });
  HeThongRap.hasMany(CumRap, {
    as: "CumRaps",
    foreignKey: "ma_cum_rap",
  });

  return {
    Banner,
    CumRap,
    DatVe,
    Ghe,
    HeThongRap,
    LichChieu,
    NguoiDung,
    Phim,
    RapPhim,
  };
}
