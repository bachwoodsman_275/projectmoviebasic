import _sequelize from "sequelize";
const { Model, Sequelize } = _sequelize;

export default class NguoiDung extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        nguoi_dung_id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
        },
        tai_khoan: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        ho_ten: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        email: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        so_dt: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        mat_khau: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        loai_nguoi_dung: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "NguoiDung",
        timestamps: false,
        indexes: [
          {
            name: "PRIMARY",
            unique: true,
            using: "BTREE",
            fields: [{ name: "nguoi_dung_id" }],
          },
        ],
      }
    );
  }
}
