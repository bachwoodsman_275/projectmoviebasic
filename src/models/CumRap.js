import _sequelize from "sequelize";
const { Model, Sequelize } = _sequelize;

export default class CumRap extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        ma_cum_rap: {
          type: DataTypes.STRING(255),
          allowNull: false,
          primaryKey: true,
        },
        ten_cum_rap: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        dia_chi: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
        ma_he_thong_rap: {
          type: DataTypes.STRING(150),
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "CumRap",
        timestamps: false,
        indexes: [
          {
            name: "PRIMARY",
            unique: true,
            using: "BTREE",
            fields: [{ name: "ma_cum_rap" }],
          },
        ],
      }
    );
  }
}
