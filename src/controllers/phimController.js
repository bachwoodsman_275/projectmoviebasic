import initModels from "./../models/init-models.js";
import { sequelize } from "../configs/connect.js";
import { responseData } from "./../configs/response.js";
import { Op } from "sequelize";
const model = initModels(sequelize);

const PhanTrang = (soTrang, soPhanTuTrenTrang) => {
  let pageSize = Number(soPhanTuTrenTrang);
  let pageId = Number(soTrang);
  let index = (pageId - 1) * pageSize;
  return { pageSize, pageId, index };
};

export const LayDanhSachPhim = async (req, res) => {
  let data = await model.Phim.findAll({});
  responseData(res, data, "Success", 200);
};

export const LayDanhSachBanner = async (req, res) => {
  let data = await model.Banner.findAll({});
  responseData(res, data, "Success", 200);
};

export const LayDanhSachPhimPhanTrang = async (req, res) => {
  let soTrang = req.query.soTrang;
  let maNhom = req.query.maNhom;
  let soPhanTuTrenTrang = req.query.soPhanTuTrenTrang;

  let { pageSize, pageId, index } = PhanTrang(soTrang, soPhanTuTrenTrang);
  let tongSoPhanTu = await model.Phim.findAll({
    offset: index,
    limit: pageSize,
  });

  responseData(res, { pageSize, pageId, tongSoPhanTu }, "Success", 200);
};

export const LayDanhSachPhimTheoNgay = async (req, res) => {
  let maNhom = req.query.maNhom;
  let soTrang = req.query.soTrang;
  let soPhanTuTrenTrang = req.query.soPhanTuTrenTrang;
  let tuNgay = req.query.tuNgay;
  let denNgay = req.query.denNgay;

  let data = await model.Phim.findAll({
    where: {
      ngay_khoi_chieu: {
        [Op.between]: [tuNgay, denNgay],
      },
    },
  });
  if (data.length == 0 || data == []) {
    responseData(res, "", "Not Found, please change the date", 404);
    return;
  }
  responseData(
    res,
    { maNhom, soTrang, soPhanTuTrenTrang, data },
    "Success",
    200
  );
};

export const ThemPhimUploadHinh = async (req, res) => {
  let { tenPhim, maPhim, trailer, moTa, danhGia, hot, dangChieu, sapChieu } =
    req.body;
  let hinhAnh = req.file.filename;
  let checkMaPhim = await model.Phim.findOne({
    where: { ma_phim: maPhim },
  });
  if (checkMaPhim) {
    responseData(res, "", "The movie is already exists", 400);
    return;
  }
  let phimMoiUploadHinh = await model.Phim.create({
    ten_phim: tenPhim,
    ma_phim: maPhim,
    trailer,
    hinh_anh: hinhAnh,
    mo_ta: moTa,
    danh_gia: danhGia,
    hot,
    dang_chieu: dangChieu,
    sap_chieu: sapChieu,
  });
  responseData(res, phimMoiUploadHinh, "Success", 200);
};
