import initModels from "../models/init-models.js";
import { sequelize } from "../configs/connect.js";
// import { Op } from "sequelize";
import { responseData } from "../configs/response.js";
const model = initModels(sequelize);

export const DatVe = async (req, res) => {
  let { nguoiDungId, maLichChieu, maGhe } = req.body;
  // let checkDatVe = await model.DatVe.findOne({
  //   where: {
  //     nguoi_dung_id: nguoiDungId,
  //     ma_lich_chieu: maLichChieu,
  //     ma_ghe: maGhe,
  //   },
  // });
  let checkNguoiDungId = await model.NguoiDung.findOne({
    where: {
      nguoi_dung_id: nguoiDungId,
    },
  });
  if (!checkNguoiDungId) {
    responseData(res, "", "User is not exist, please sign up", 404);
    return;
  }
  let checkMaLichChieu = await model.LichChieu.findOne({
    where: {
      ma_lich_chieu: maLichChieu,
    },
  });
  if (!checkMaLichChieu) {
    responseData(res, "", "Schedule does not exists", 404);
    return;
  }
  let checkGhe = await model.Ghe.findOne({
    where: {
      ma_ghe: maGhe,
    },
  });
  if (!checkGhe) {
    responseData(res, "", "This seat is not exists", 404);
    return;
  }

  await model.DatVe.create({
    nguoi_dung_id: nguoiDungId,
    ma_lich_chieu: maLichChieu,
    ma_ghe: maGhe,
  });
  responseData(res, { nguoiDungId, maLichChieu, maGhe }, "Success", 200);
};
export const LayDanhSachPhongVe = async (req, res) => {
  let data = await model.DatVe.findAll({});
  responseData(res, data, "Success", 200);
};
export const TaoLichChieu = async (req, res) => {};
