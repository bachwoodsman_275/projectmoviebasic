import initModels from "../models/init-models.js";
import { sequelize } from "../configs/connect.js";
import { responseData } from "../configs/response.js";

const model = initModels(sequelize);

export const LayDanhSachLichChieu = async (req, res) => {
  let data = await model.LichChieu.findAll({
    include: ["ma_phim_Phim", "ma_rap_RapPhim"],
  });
  responseData(res, data, "Success", 200);
};

export const LayThongTinHeThongRap = async (req, res) => {
  let maHeThongRap = req.query.maHeThongRap;

  let data = await model.HeThongRap.findOne({
    where: { ma_he_thong_rap: maHeThongRap },
  });
  responseData(res, data, "Success", 200);
};
export const LayThongTinCumRapTheoHeThong = async (req, res) => {
  let maHeThongRap = req.query.maHeThongRap;

  let data = await model.CumRap.findAll({
    where: { ma_he_thong_rap: maHeThongRap },
    include: ["danhSachRap"],
    // attributes: { exclude: ["ma_he_thong_rap"] },
  });

  responseData(res, data, "Success", 200);
};

export const ThemCumRap = async (req, res) => {
  let { maCumRap, tenCumRap, diaChi } = req.body;
  let [cumRap, created] = await model.CumRap.findOrCreate({
    where: { ma_cum_rap: maCumRap },
    defaults: {
      ma_cum_rap: maCumRap,
      ten_cum_rap: tenCumRap,
      dia_chi: diaChi,
    },
  });
  // Nếu không tìm thấy maCumRap trong db thì create
  if (created) {
    responseData(res, cumRap, "Create Success", 200);
  } else {
    // Nếu tồn tại trong table => update cumRap
    await cumRap.update({
      ten_cum_rap: tenCumRap,
      dia_chi: diaChi,
    });
    responseData(res, cumRap, "Update Success", 200);
  }
};
