import initModels from "./../models/init-models.js";
import { sequelize } from "./../configs/connect.js";
import { responseData } from "./../configs/response.js";
import { Op } from "sequelize";
import bcrypt from "bcrypt";
const model = initModels(sequelize);

export const LayDanhSachLoaiNguoiDung = async (req, res) => {
  let tenLoai = await model.NguoiDung.findAll({
    attributes: ["loai_nguoi_dung"],
  });
  // Loại bỏ các giá trị trùng lặp
  const uniqueValues = [
    ...new Set(tenLoai.map((item) => JSON.stringify(item))),
  ].map(JSON.parse);
  responseData(res, uniqueValues, "Success", 200);
};

export const DangNhap = async (req, res) => {
  let { taiKhoan, matKhau } = req.body;
  let checkTaiKhoan = await model.NguoiDung.findOne({
    where: {
      tai_khoan: taiKhoan,
    },
  });
  console.log("checkTaiKhoan :", checkTaiKhoan);
  if (!checkTaiKhoan) {
    responseData(res, "", "Account is not exists, please sign up", 400);
    return;
  } else {
    var checkMatKhau = bcrypt.compareSync(
      matKhau,
      checkTaiKhoan.dataValues.mat_khau
    );
    if (!checkMatKhau) {
      responseData(res, "", "Password is not valid", 404);
      return;
    }
  }
  responseData(res, "", "Success", 200);
};
export const DangKi = async (req, res) => {
  let { taiKhoan, matKhau, email, soDt, maNhom, hoTen } = req.body;
  let checkEmail = await model.NguoiDung.findOne({
    where: {
      email: email,
    },
  });
  // checkEmail ton tai => return
  if (checkEmail) {
    responseData(res, "", "Email is already exists", 400);
    return;
  } else {
    var newAccount = {
      tai_khoan: taiKhoan,
      mat_khau: bcrypt.hashSync(matKhau, 10),
      email,
      so_dt: soDt,
      ma_nhom: maNhom,
      ho_ten: hoTen,
    };
    await model.NguoiDung.create(newAccount);
    responseData(res, newAccount, "Success", 200);
  }
};
export const LayDanhSachNguoiDung = async (req, res) => {
  let data = await model.NguoiDung.findAll({});
  responseData(res, data, "Success", 200);
};

export const TimKiemNguoiDung = async (req, res) => {
  let tuKhoa = req.query.tuKhoa;
  let data;
  data = await model.NguoiDung.findAll({
    where: {
      ho_ten: { [Op.like]: `%${tuKhoa}%` },
    },
  });

  // nếu data có giá trị => hiển thị kết quả
  // nếu data giá trị bằng rỗng => hiển thị tất cả
  if (data.length === 0) {
    data = await model.NguoiDung.findAll();
    responseData(res, data, "Not found", 401);
    return;
  }
  responseData(res, data, "Success", 200);
};

export const CapNhatThongTinNguoiDung = async (req, res) => {
  let { taiKhoan, email, soDt, matKhau, loaiNguoiDung, hoTen } = req.body;
  if (!email) {
    responseData(res, "", "Please fill out email", 400);
    return;
  }
  let checkEmail = await model.NguoiDung.findOne({
    where: {
      email: email,
    },
  });
  if (!checkEmail) {
    responseData(res, "", "Email is not exists", 404);
    return;
  }
  let updateNguoiDung = {
    ...checkEmail.dataValues,
    tai_khoan: taiKhoan,
    mat_khau: matKhau,
    loai_nguoi_dung: loaiNguoiDung,
    ho_ten: hoTen,
    so_dt: soDt,
  };
  await model.NguoiDung.update(updateNguoiDung, {
    where: { email: checkEmail.dataValues.email },
  });
  responseData(res, updateNguoiDung, "Success", 200);
};

export const XoaNguoiDung = async (req, res) => {
  let { taiKhoan, matKhau } = req.params;

  let checkTaiKhoan = await model.NguoiDung.findOne({
    where: {
      tai_khoan: taiKhoan,
    },
  });
  if (!checkTaiKhoan) {
    responseData(res, "", "Account is not exists", 404);
    return;
  }
  if (!checkTaiKhoan.dataValues.mat_khau == matKhau) {
    responseData(res, "", "Password is not valid", 400);
    return;
  }
  await model.NguoiDung.destroy({
    where: {
      tai_khoan: checkTaiKhoan.dataValues.tai_khoan,
    },
  });
  responseData(res, "", "success", 200);
};
